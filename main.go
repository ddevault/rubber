package main

import (
	"fmt"
	"net"
	"os"
	"github.com/SirCmpwn/rubber/client"
	. "github.com/SirCmpwn/rubber/config"
)

var config Config

func handleConnection(client net.Conn) {
	fmt.Println("Connection from: ", client.RemoteAddr())
	var buffer [512]byte
	for {
		length, err := client.Read(buffer[0:])
		if err != nil {
			fmt.Println("Connection ", client.RemoteAddr(), " closed.")
			return
		}
		fmt.Print(string(buffer[0:length]))
		client.Write(buffer[0:length])
	}
}

func main() {
	if err := ReadConfig("config.ini", &config); err != nil {
		fmt.Printf("err: %s\n", err)
		os.Exit(1)
	}
	fmt.Printf("Connecting to %s...\n", config.Network.Name)
	connection, err := net.Dial("tcp", config.Network.Address)
	if err != nil {
		fmt.Printf("Failed to connect: %s\n", err)
		os.Exit(1)
	}
	client := client.NewConnection(connection)
	client.StartWorker(config)
	listener, err := net.Listen("tcp", fmt.Sprint(":", config.Listen.Port))
	if err != nil {
		fmt.Printf("Error: %s\n", err)
		os.Exit(1)
	}
	fmt.Println("Listening on ", config.Listen.Port)
	for {
		connection, err := listener.Accept()
		if err != nil {
			fmt.Printf("Error: %s", err)
			continue
		}
		go handleConnection(connection)
	}
	os.Exit(0)
}
