package client

import (
	"net"
	"fmt"
	"strings"
	. "github.com/SirCmpwn/rubber/config"
)

var config Config

type IrcMessage struct {
	RawMessage string
	Prefix string
	Command string
	Parameters []string
}

type IrcConnection struct {
	Connection net.Conn
	Buffer [512]byte
	PumpIn chan IrcMessage
	PumpOut chan string
}

func ParseMessage(message string) IrcMessage {
	// See section 2.3.1 of RFC 1459 for message format details
	m := IrcMessage{}
	m.RawMessage = message
	if len(message) == 0 {
		return m
	}
	if message[0] == ':' {
		i := strings.Index(message, " ")
		m.Prefix = message[1:i]
		message = message[i + 1:]
	}
	if len(message) == 0 {
		return m
	}
	m.Parameters = []string{}
	space := strings.Index(message, " ")
	m.Command = strings.ToUpper(message[:space])
	message = message[space + 1:]
	for ; len(message) != 0; {
		if message[0] == ':' {
			m.Parameters = append(m.Parameters, message[1:])
			break
		} else {
			i := strings.Index(message, " ")
			if i == -1 {
				i = len(message)
			}
			m.Parameters = append(m.Parameters, message[0:i])
			if i + 1 >= len(message) {
				break
			}
			message = message[i + 1:]
		}
	}
	return m
}

func NewConnection(connection net.Conn) *IrcConnection {
	c := new(IrcConnection)
	c.Connection = connection
	c.PumpIn = make(chan IrcMessage)
	c.PumpOut = make(chan string)
	return c
}

func (c *IrcConnection) StartWorker(_config Config) {
	config = _config
	go networkWorker(c)
	go handleMessages(c)
	handshake(c)
}

func (c *IrcConnection) Send(format string, arguments ...interface{}) {
	c.PumpOut <- fmt.Sprintf(format, arguments...)
}

func handshake(c *IrcConnection) {
	if len(config.Network.Password) > 0 {
		c.Send("PASS :%s", config.Network.Password)
	}
	c.Send("NICK %s", config.Network.Nick)
	c.Send("USER %s %s %s :%s", config.Network.User, config.Network.User, config.Network.Address, config.Network.RealName)
}

func networkWorker(c *IrcConnection) {
	var data string = ""
	for {
		length, err := c.Connection.Read(c.Buffer[:])
		if err != nil {
			fmt.Println("Connection ", c.Connection.RemoteAddr(), " closed.")
			return
		}
		data += string(c.Buffer[:length])
		crlf := strings.Index(data, "\r\n")
		for ; crlf != -1; {
			c.PumpIn <- ParseMessage(data[:crlf])
			data = data[crlf + 2:]
			crlf = strings.Index(data, "\r\n")
		}
	}
}

func handleMessages(c *IrcConnection) {
	for {
		select {
		case message := <-c.PumpIn:
			fmt.Printf("<< %s\n", message.RawMessage)
		case message := <-c.PumpOut:
			fmt.Printf(">> %s\n", message)
			c.Connection.Write([]byte(message + "\r\n"))
		}
	}
}
